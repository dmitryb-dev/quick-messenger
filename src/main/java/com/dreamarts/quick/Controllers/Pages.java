package com.dreamarts.quick.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Pages {

    @RequestMapping("app/**")
    public String app() {
        return "app";
    }

    @RequestMapping("/")
    public String index() {
        return "redirect:/app";
    }

    @RequestMapping("/app")
    public String indexapp() {
        return "app";
    }
}
