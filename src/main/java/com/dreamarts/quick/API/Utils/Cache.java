package com.dreamarts.quick.API.Utils;

import java.util.HashMap;
import java.util.Map;

public class Cache<K, V> {

    public void put(K key, V value) {
        cache.put(key, value);
    }
    public V cached(K key) {
        return cache.get(key);
    }
    public V cachedOrNew(K key, Factory<V, K> factory) {
        V cached = cache.get(key);
        if (cached == null)
            cache.put(key, cached = factory.instance(key));
        return cached;
    }
    public interface Factory<V, P> {
        V instance(P param);
    }

    private Map<K, V> cache = new HashMap<>();
}
