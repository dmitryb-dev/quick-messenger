package com.dreamarts.quick.API.Utils.Validation;

import com.dreamarts.quick.API.Utils.Cache;

import java.util.regex.Pattern;

public class Constrains {
    public static final Constrain<CharSequence>
            NOT_EMPTY = string -> string != null && string.length() > 0,
            EMAIL = string -> Constrains.emailRegex.matcher(string).matches(),
            HAS_NUMBER = string -> string.chars().anyMatch(Character::isDigit),
            WITHOUT_SPACE = string -> !string.chars().anyMatch(Character::isSpaceChar),
            HAS_UPPER = string -> string.chars().anyMatch(Character::isUpperCase);

    public static final Constrain NOT_NULL = object -> object != null;

    public static Constrain<Object> MIN_LENGTH(int length) {
        return minLengthCache.cachedOrNew(length, minLengthConstrain);
    }

    public static Constrain<Object> MAX_LENGTH(int length) {
        return maxLengthCache.cachedOrNew(length, maxLengthConstrain);
    }

    public static Constrain<Comparable> MIN(Comparable value) {
        return minCache.cachedOrNew(value, minConstrain);
    }
    public static Constrain<Comparable> MAX(Comparable value) {
        return maxCache.cachedOrNew(value, maxConstrain);
    }

    public static Constrain INVERT(Constrain constrain) {
        return invertCache.cachedOrNew(constrain, invertConstrain);
    }


    private static final Cache.Factory<Constrain<Object>, Integer>
            minLengthConstrain = length -> object -> object.toString().length() >= length,
            maxLengthConstrain = length -> object -> object.toString().length() <= length;
    private static final Cache<Integer, Constrain<Object>>
            minLengthCache = new Cache<>(),
            maxLengthCache = new Cache<>();

    private static final Cache.Factory<Constrain<Comparable>, Object>
            minConstrain = toCompare -> comparable -> comparable.compareTo(toCompare) >= 0,
            maxConstrain = toCompare -> comparable -> comparable.compareTo(toCompare) <= 0;
    private static final Cache<Object, Constrain<Comparable>>
            maxCache = new Cache<>(),
            minCache = new Cache<>();

    private static final Cache.Factory<Constrain, Constrain>
            invertConstrain = toInvert -> object -> !toInvert.test(object);
    private static final Cache<Constrain, Constrain> invertCache = new Cache<>();

    private static final Pattern emailRegex =
        Pattern.compile(".+@.+\\..+");
}
