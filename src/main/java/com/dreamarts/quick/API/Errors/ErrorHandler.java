package com.dreamarts.quick.API.Errors;

import com.dreamarts.quick.API.APIResult;
import com.dreamarts.quick.API.Errors.Presets.CommonErrors;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
    @ResponseBody String error400() {
        return APIResult.wrapOK(CommonErrors.WRONG_PARAMETER.toJSON());
    }


}
