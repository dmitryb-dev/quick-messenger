package com.dreamarts.quick.API.Utils.Properties;

import com.dreamarts.quick.API.Utils.Properties.Sources.CastPropertiesDecorator;
import com.dreamarts.quick.API.Utils.Properties.Sources.PropertiesSource;
import sun.plugin.dom.exception.InvalidStateException;

import java.util.HashMap;
import java.util.Map;

public class From {

    public static CastPropertiesDecorator source(String name) {
        return sources.get(name);
    }

    public static CastPropertiesDecorator source() {
        if (defaultSourceAlias == null)
            throw new InvalidStateException("Default source isn't specified. Please call setDefault() method");
        return source(defaultSourceAlias);
    }

    public static void append(String alias, PropertiesSource source) {
        sources.put(alias, new CastPropertiesDecorator(source));
    }

    public static void saveAll() {
        sources.forEach((s, propertiesSource) -> propertiesSource.save());
    }

    public static void setDefault(String alias) {
        defaultSourceAlias = alias;
    }

    private static Map<String, CastPropertiesDecorator> sources = new HashMap<>();
    private static String defaultSourceAlias;
}
