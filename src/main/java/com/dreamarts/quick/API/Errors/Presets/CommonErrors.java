package com.dreamarts.quick.API.Errors.Presets;

import com.dreamarts.quick.API.Errors.Core.AbstractErrors;
import com.dreamarts.quick.API.Errors.Core.Error;
import com.dreamarts.quick.API.Errors.Core.ErrorType;
import com.dreamarts.quick.API.Utils.Cache;

public class CommonErrors extends AbstractErrors {
    static {
        type = ErrorType.Server;
    }
    public static Error internal(String message) {
        return cache.cachedOrNew(message, errorFactory);
    }
    public static Error
            WRONG_PARAMETER = error(001, "Request", "Not enought or wrong parameters");
    public static Error
            CANNOT_SEND_EMAIL = error(002, "Unknown", "Can't send activation code to your email...");

    private static Cache<String, Error> cache = new Cache<>();
    private static Cache.Factory<Error, String> errorFactory = message -> error(000, "Internal error", message);
}
