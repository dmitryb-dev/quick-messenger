package com.dreamarts.quick.API.Utils.Properties.Sources;

import com.dreamarts.quick.API.Utils.Properties.Parsers.InvalidFormatException;
import com.dreamarts.quick.API.Utils.Properties.Parsers.Parser;
import com.dreamarts.quick.API.Utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

public class ResourceSource implements PropertiesSource {
    public ResourceSource(String nameOfResource, Parser parser) {
        this.parser = parser;

        try {
            String file = Utils.readStream(getClass().getClassLoader().getResourceAsStream(nameOfResource));
            values = parser.parse(file);
        } catch (InvalidFormatException e) {
            throw new InvalidSourceException(e);
        }

        resourceName = nameOfResource;
    }

    @Override
    public <T> T get(String name) {
        return (T) values.get(name);
    }

    @Override
    public void set(String name, Object value) {
        values.remove(name);
        values.put(name, value);
    }

    @Override
    public void save() {
        try {
            new FileOutputStream(
                    new File(getClass().getClassLoader().getResource(resourceName).toURI()))
                    .write(parser.format(values).getBytes());
        } catch (Exception e) {
           throw new InvalidSourceException(e);
        }
    }

    public String getResourceName() {
        return resourceName;
    }

    private Map<String, Object> values;
    private String resourceName;
    private Parser parser;


}
