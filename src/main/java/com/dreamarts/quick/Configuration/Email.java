package com.dreamarts.quick.Configuration;

import com.dreamarts.quick.API.Utils.Properties.From;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class Email {

    @Bean
    public JavaMailSender localMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        mailSender.setJavaMailProperties(props);

        mailSender.setHost(host);
        mailSender.setUsername(user);
        mailSender.setPassword(pass);
        mailSender.setPort(port);
        return mailSender;
    }

    private String
            host = From.source("mail").get("host"),
            user = From.source("mail").get("user"),
            pass = From.source("mail").get("pass");
    private int
            port = From.source("mail").getInt("port");
}
