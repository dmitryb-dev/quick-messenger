package com.dreamarts.quick.API.Services.Auth;

import com.dreamarts.quick.API.DAO.Interfaces.SessionDAO;
import com.dreamarts.quick.API.Services.Auth.AuthenticationObjects.Token;
import com.dreamarts.quick.API.Services.Auth.AuthenticationObjects.TokenAuthentication;
import com.dreamarts.quick.Model.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class TokenAuthProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = (String) authentication.getCredentials();
        if (token == null) throw new BadCredentialsException("No token");

        Session session = sessionDAO.findByToken(token);
        if (session == null) throw new WrongSessionException("Not such session " + token);
        if (session.getExpires().before(new java.sql.Timestamp(System.currentTimeMillis()))) {
            sessionDAO.expire(session);
            throw new WrongSessionException("Session expired " + token);
        }

        return new TokenAuthentication(session.getToken(),
                session.getUser(),
                session,
                new SimpleGrantedAuthority(session.getRole().toString()));
    }

    @Autowired
    private SessionDAO sessionDAO;
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass == Token.class;
    }
}
