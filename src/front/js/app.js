"use strict";
var PagesFactory_1 = require("../node_modules/pagesfw/js/PagesFactory");
var Animation_1 = require("../node_modules/pagesfw/js/core/Pages/Animation");
var pages = PagesFactory_1.default.fromDocument(PagesFactory_1.default.slashMode("app"));
pages.container('account').page('Sign Up').to('Sign In').animation =
    new Animation_1.Animation('fadeInLeft animated', 'fadeOutRight animated');
pages.container('main').page('Account Info').to('Authorization').animation =
    new Animation_1.Animation('rotateIn animated', 'fadeOutDown animated');
/*
 const exploders = new ExplodersParser();
exploders.from(document);
const exploder = exploders.get("info");
exploder.onLoad = () => pages.parse(exploder.container);
pages.container('main').page('Account Info').onSelect = () => exploder.explode();
*/
pages.setIndex('Authorization/Sign In');
pages.start();
