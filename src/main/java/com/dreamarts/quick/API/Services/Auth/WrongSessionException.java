package com.dreamarts.quick.API.Services.Auth;

import org.springframework.security.core.AuthenticationException;

public class WrongSessionException extends AuthenticationException {
    public WrongSessionException(String s) {
        super(s);
    }
}
