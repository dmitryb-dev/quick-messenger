var path = require("path");
var extractTextPlugin = require('extract-text-webpack-plugin');
module.exports = {
    devtool: 'source-map',
    context: path.join(__dirname, ''),
    entry: {
        app: './js/app.js',
        styles: './css/main.scss'
    },
    output: {
        path: path.join(__dirname, '../main/webapp/js/'),
        filename: "[name].js"
    },
    module: {
        loaders: [
            { test: /(\.tsx$|\.ts$|\.js.map$)/, loader: 'ignore-loader' },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.scss$/,
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.scss$/,
                loader: extractTextPlugin.extract('style-loader', 'css-loader!resolve-url!sass-loader?sourceMap')
            },
            {
                test: /\.css$/,
                loader: extractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /\.woff2?$|\.ttf$|\.eot$|\.svg$|\.png|\.jpe?g|\.gif$/,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [
        new extractTextPlugin('../css/styles.css', {
            allChunks: true
        })
    ],
    sassLoader: {
        includePaths: [path.resolve(__dirname, "./css")]
    }
}