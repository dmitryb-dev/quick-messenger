package com.dreamarts.quick.API.Utils.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HTMLMailSenderImpl implements HTMLMailSender {

    public boolean send(String to, String html) {
        return emailSender.send(to, html, getTitle(html), true);
    }

    public String getTitle(String html) {
        Matcher matcher = Pattern.compile("(?<=<title>).*(?=<\\/title)").matcher(html);
        if (matcher.find())
            return matcher.group();
        return "";
    }

    @Autowired
    EmailSender emailSender;
}
