package com.dreamarts.quick.API.Services.Auth.AuthenticationObjects;

import com.dreamarts.quick.Model.Access;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

public class TokenAuthentication<P, D> implements Authentication {
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(authority);
    }

    public TokenAuthentication(String token, P user, D details, GrantedAuthority authority) {
        this.token = token;
        this.user = user;
        this.authority = authority;
        this.details = details;
        isAuthenticated = true;
    }

    public TokenAuthentication() {
        isAuthenticated = false;
        authority = new SimpleGrantedAuthority(Access.ROLE_ANONYMOUS.toString());
    }

    @Override
    public String getCredentials() {
        return token;
    }

    @Override
    public D getDetails() {
        return details;
    }

    @Override
    public P getPrincipal() {
        return user;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        isAuthenticated = b;
    }

    @Override
    public String getName() {
        return "TokenAuthentication authentication";
    }

    private String token;
    private boolean isAuthenticated;
    private P user;
    private D details;
    private GrantedAuthority authority;
}
