package com.dreamarts.quick;

import com.dreamarts.quick.API.DAO.Interfaces.SessionDAO;
import com.dreamarts.quick.API.Services.Interfaces.AccountService;
import com.dreamarts.quick.Model.Access;
import com.dreamarts.quick.Model.Session;
import com.dreamarts.quick.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;

@Component
public class ApplicationTest {

    public static void main(String[] args) {
        new Application().main(args);
    }

    @PostConstruct
    public void bootstrap() {
        users();
        sessions();
    }
    public void users() {
        accountService.create("test@test.com", passwordEncoder.encode("test12"));
    }
    public void sessions() {
        User testUser = accountService.byEmail("test@test.com");
        sessionDAO.create(new Session(
                testUser,
                "test",
                new Timestamp(System.currentTimeMillis() + 1000 * 60 * 60),
                Access.ROLE_USER
                ));
        sessionDAO.create(new Session(
                testUser,
                "admin",
                new Timestamp(System.currentTimeMillis() + 1000 * 60 * 60),
                Access.ROLE_ADMIN
        ));
        sessionDAO.create(new Session(
                testUser,
                "anon",
                new Timestamp(System.currentTimeMillis() + 1000 * 60 * 60),
                Access.ROLE_ANONYMOUS
        ));
    }

    @Autowired
    private AccountService accountService;
    @Autowired
    private SessionDAO sessionDAO;
    @Autowired
    private PasswordEncoder passwordEncoder;
}