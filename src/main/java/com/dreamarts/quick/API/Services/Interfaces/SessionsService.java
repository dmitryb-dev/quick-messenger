package com.dreamarts.quick.API.Services.Interfaces;

import com.dreamarts.quick.Model.Access;
import com.dreamarts.quick.Model.Session;
import com.dreamarts.quick.Model.User;

public interface SessionsService {
    Session startNew(User u, Access access, int validityDays);
    void expire(Session s);
}
