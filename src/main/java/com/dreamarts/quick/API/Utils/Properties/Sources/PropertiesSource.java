package com.dreamarts.quick.API.Utils.Properties.Sources;

public interface PropertiesSource {
    <T> T get(String name);
    void set(String name, Object value);
    void save();
}
