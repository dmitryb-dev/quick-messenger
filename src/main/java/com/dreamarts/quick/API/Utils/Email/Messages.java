package com.dreamarts.quick.API.Utils.Email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

@Component
public class Messages {

    public Messages() {
    }

    public String confirmationMessage(String link, Locale locale) {
        Context context = new Context(locale);
        context.setVariable("link", link);
        return templateEngine.process("Mail/MailConfirmation", context);
    }

    @Autowired
    private TemplateEngine templateEngine;
}
