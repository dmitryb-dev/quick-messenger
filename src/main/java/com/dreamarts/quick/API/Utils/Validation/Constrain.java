package com.dreamarts.quick.API.Utils.Validation;

public interface Constrain<T> {
    boolean test(T object);
}
