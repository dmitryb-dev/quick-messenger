package com.dreamarts.quick.API.DAO.Interfaces;

import com.dreamarts.quick.Configuration.PropertiesConfiguration;
import com.dreamarts.quick.Model.NonActivated;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class NonActivatedDAOTest {

    static {
        PropertiesConfiguration.init();
    }

    @Test
    public void daoTest() {
        nonActivatedDAO.save(new NonActivated("email", "pass", "123", 123));
        Assert.assertNotNull(nonActivatedDAO.findOne("email"));
    }

    @Autowired
    private NonActivatedDAO nonActivatedDAO;
}