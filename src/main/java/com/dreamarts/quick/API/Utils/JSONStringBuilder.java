package com.dreamarts.quick.API.Utils;

public class JSONStringBuilder {

    /*
     * @keyValue pairs key-value. Example, if you want to write:
     * "key1" = "1", "key2" = "String"
     * yoy need to call like this object ("name", "key1", 1, "key2", "String");
     */
    public static String object(String objectName, String... keyValue) {
        JSONStringBuilder builder = new JSONStringBuilder(1000);
        builder.startObject(objectName);
        for (int i = 0; i < keyValue.length; i = i + 2)
            builder.property(keyValue[i], keyValue[i + 1]);
        return builder.endObject().toString();
    }

    public static String value(String name, String value) {
        return new StringBuilder()
                .append('"').append(name).append("\": ")
                .append('"').append(value).append('"')
                .toString();
    }

    public JSONStringBuilder(int capacity) {
        this.builder = new StringBuilder(capacity);
    }

    public JSONStringBuilder startObject() {
        separateIfNeed();
        builder.append(INDENTS[currentIndent++]).append("{\n");
        isNewObjectStarted = true;
        return this;
    }
    public JSONStringBuilder startObject(String name) {
        if (name == null) return startObject();
        separateIfNeed();
        builder.append(INDENTS[currentIndent++]).append('"').append(name).append("\": {\n");
        isNewObjectStarted = true;
        return this;
    }

    public JSONStringBuilder endObject() {
        builder.append("\n").append(INDENTS[--currentIndent]).append("}");
        return this;
    }

    public JSONStringBuilder rawText(String text) {
        separateIfNeed();
        builder
                .append(INDENTS[currentIndent])
                .append(text.replaceAll("\n", "\n" + INDENTS[currentIndent]));
        return this;
    }

    public JSONStringBuilder property(String name, String value) {
        startProperty(name);
        builder.append('"').append(value).append('"');
        return this;
    }
    public JSONStringBuilder property(String name, int value) {
        startProperty(name);
        builder.append(value);
        return this;
    }

    public String toString() {
        return builder.toString();
    }

    private void startProperty(String name) {
        separateIfNeed();
        builder.append(INDENTS[currentIndent]).append('"').append(name).append("\": ");
    }

    private void separateIfNeed() {
        if (!isNewObjectStarted) {
            builder.append(",\n");
        }
        isNewObjectStarted = false;
    }

    private StringBuilder builder;
    private int currentIndent;
    private boolean isNewObjectStarted = true;
    private final static String[] INDENTS = {
            "", "  ", "    ", "      ", "        ", "          "
    };
}
