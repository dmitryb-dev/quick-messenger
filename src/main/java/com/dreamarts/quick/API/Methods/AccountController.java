package com.dreamarts.quick.API.Methods;

import com.dreamarts.quick.API.APIInfo;
import com.dreamarts.quick.API.APIResult;
import com.dreamarts.quick.API.Errors.Core.Error;
import com.dreamarts.quick.API.Errors.Presets.AccountErrors;
import com.dreamarts.quick.API.Errors.Presets.CommonErrors;
import com.dreamarts.quick.API.Services.Interfaces.AccountService;
import com.dreamarts.quick.API.Services.Interfaces.NonActivatedService;
import com.dreamarts.quick.API.Services.Interfaces.SessionsService;
import com.dreamarts.quick.API.Utils.Email.HTMLMailSender;
import com.dreamarts.quick.API.Utils.Email.Messages;
import com.dreamarts.quick.API.Utils.JSONStringBuilder;
import com.dreamarts.quick.API.Utils.Validation.Check;
import com.dreamarts.quick.Model.Access;
import com.dreamarts.quick.Model.NonActivated;
import com.dreamarts.quick.Model.Session;
import com.dreamarts.quick.Model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import static com.dreamarts.quick.API.Utils.Validation.Constrains.*;

@RestController()
@RequestMapping(path = APIInfo.PATH + "account/")
public class AccountController {

    @RequestMapping("signin")
    public String signIn(String email, String pass) {
        User user = accountService.byEmail(email);

        if (user == null) return APIResult.wrapError(AccountErrors.WRONG_EMAIL.toJSON());
        if (!passwordEncoder.matches(pass, user.getPass()))
            return APIResult.wrapError(AccountErrors.WRONG_PASS.toJSON());

        final int EXPIRATION_TIME_DAYS = 100;
        Session session = sessionsService.startNew(user, Access.ROLE_USER, EXPIRATION_TIME_DAYS);
        return APIResult.wrapOK(
                JSONStringBuilder.object("session",
                        "userId", Long.toString(user.getId()),
                        "token", session.getToken(),
                        "validityDays", Integer.toString(EXPIRATION_TIME_DAYS))
        );
    }

    @RequestMapping(value = "activate", params = "code")
    public String activate(@RequestParam("code") String confirmationCode, @RequestParam("email") String email) {
        NonActivated activated = nonActivatedService.activate(email, confirmationCode);
        if (activated == null) return APIResult.wrapError(AccountErrors.WRONG_CONFIRMATION_CODE.toJSON());
        accountService.create(activated.getEmail(), activated.getPass());
        return APIResult.RESULT_OK;
    }

    @RequestMapping("access")
    public String access(@RequestParam(required = false, defaultValue = "user") String accessString) {
        return "NOT_IMPLEMENTED";
    }

    @RequestMapping("signout")
    public String signOut() {
        Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (!(details instanceof Session)) return APIResult.wrapError(CommonErrors.internal("Can't load session").toJSON());

        sessionsService.expire((Session) details);
        return APIResult.wrapOK(JSONStringBuilder.value("comment", "Session was finished"));
    }

    @RequestMapping(value = "signup", params = {"email", "pass"})
    public String signUp(@RequestParam(defaultValue = "") String email,
                         @RequestParam(defaultValue = "") String pass,
                         HttpServletRequest request) {
        log.debug("Someone trying to sign up, email = {}, pass = {}", email, pass);
        Error error = checkSignUpData(email, pass);
        if (error != null) {
            log.debug("Data contains error: {}", error);
            return APIResult.wrapError(error.toJSON());
        }

        String confirmationCode = nonActivatedService.create(email, passwordEncoder.encode(pass));
        if (!emailSender.send(email,
                messages.confirmationMessage(confirmationRequest(request, confirmationCode, email), request.getLocale())))
            return APIResult.wrapError(CommonErrors.CANNOT_SEND_EMAIL.toJSON());
        log.debug("Confirmation code '{}' is sent to {}", confirmationCode, email);

        return APIResult.wrapOK(
                JSONStringBuilder.object("user",
                        "email", email,
                        "pass", pass)
        );
    }

    private Error checkSignUpData(String email, String pass) {
        if (Check.one(email, NOT_EMPTY, EMAIL) != null) return AccountErrors.WRONG_EMAIL_FORMAT;
        if (accountService.byEmail(email) != null) return AccountErrors.EMAIL_EXISTS;

        Error passError = Check.one(pass,
                MIN_LENGTH(6), AccountErrors.SHORT_PASSWORD,
                HAS_NUMBER, AccountErrors.MUST_HAVE_NUMBER);
        return passError;
    }

    private String confirmationRequest(HttpServletRequest request, String code, String email) {
        try {
            return new URI(request.getRequestURL().toString()).resolve(".").toString() +
                    "activate?code=" + code +
                    "&email=" + email;
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Autowired
    private AccountService accountService;
    @Autowired
    private NonActivatedService nonActivatedService;
    @Autowired
    private SessionsService sessionsService;
    @Autowired
    private HTMLMailSender emailSender;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private Messages messages;

    private static final Logger log = LoggerFactory.getLogger(AccountController.class);
}
