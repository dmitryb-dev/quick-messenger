package com.dreamarts.quick.API.DAO.Interfaces;

public interface NonActivatedDAOExtension {
    void expire();
}
