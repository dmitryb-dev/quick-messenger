/// <reference path="../typings/react/react.d.ts" />
/// <reference path="../typings/react/react-dom.d.ts" />

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Welcome} from './welcome'

ReactDOM.render(React.createElement(Welcome), document.getElementById("main"));