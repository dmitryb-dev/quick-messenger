package com.dreamarts.quick.API.Services;

import com.dreamarts.quick.API.DAO.Interfaces.NonActivatedDAO;
import com.dreamarts.quick.API.DAO.Interfaces.NonActivatedDAOExtension;
import com.dreamarts.quick.API.Services.Interfaces.NonActivatedService;
import com.dreamarts.quick.API.Utils.Utils;
import com.dreamarts.quick.Model.NonActivated;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class NonActivatedServiceImpl implements NonActivatedService, InitializingBean {

    @Override
    public String create(String email, String pass) {
        final int LIFETIME_DEFAULT_SEC = 60 * 15;
        NonActivated user = new NonActivated(email, pass, Utils.randomString(50), LIFETIME_DEFAULT_SEC);
        nonActivatedDAO.save(user);
        return user.getActivationCode();
    }

    @Override
    public NonActivated activate(String email, String code) {
        NonActivated found = nonActivatedDAO.findOne(email);
        if (found != null && found.getActivationCode().equals(code)) {
            nonActivatedDAO.delete(found);
            return found;
        }
        return null;
    }

    @Override
    public void afterPropertiesSet() {
        thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(updateTimeMin * 60 * 1000);
                    fullUpdate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void fullUpdate() {
        nonActivatedDAOExtension.expire();
    }
    private int updateTimeMin = 60 * 12;

    @Autowired
    private NonActivatedDAO nonActivatedDAO;
    @Autowired
    private NonActivatedDAOExtension nonActivatedDAOExtension;
    private Thread thread;
}
