package com.dreamarts.quick.API.Utils;

import java.io.*;
import java.util.Random;

public class Utils {
    public static String randomString(int length) {
        final String values = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        final Random random = new Random(System.currentTimeMillis());

        char[] result = new char[length];
        for (int i = 0; i < result.length; i++)
            result[i] = values.charAt(random.nextInt(values.length()));

        return new String(result);
    }

    public static String readStream(InputStream stream) {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = null;
        try {
            in = new InputStreamReader(stream, "UTF-8");
        } catch (UnsupportedEncodingException e) {}
        int read = 0;
        while (true) {
            try {
                read = in.read(buffer, 0, buffer.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (read < 0)
                break;
            out.append(buffer, 0, read);
        }
        return out.toString();
    }

    public static int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
}
