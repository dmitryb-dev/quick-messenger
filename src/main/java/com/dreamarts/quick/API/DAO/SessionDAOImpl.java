package com.dreamarts.quick.API.DAO;

import com.dreamarts.quick.API.DAO.Interfaces.SessionDAO;
import com.dreamarts.quick.API.Utils.Queries;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
public class SessionDAOImpl implements SessionDAO {

    @Override
    @Transactional(readOnly = true)
    public com.dreamarts.quick.Model.Session findByToken(String token) {
        Session session = (Session) entityManager.getDelegate();
        return Queries.firstOrNull(
                session.createQuery("from Session s where s.token = '" + token + "'")
        );
    }

    @Override
    @Transactional
    public void expire(com.dreamarts.quick.Model.Session s) {
        Session session = (Session) entityManager.getDelegate();
        session.delete(s);
    }

    @Override
    @Transactional
    public void create(com.dreamarts.quick.Model.Session s) {
        Session session = (Session) entityManager.getDelegate();
        session.save(s);
    }

    @Autowired
    private EntityManager entityManager;
}
