package com.dreamarts.quick.API.Utils.Properties.Sources;

import javax.validation.constraints.NotNull;

public class CastPropertiesDecorator implements PropertiesSource {

    public CastPropertiesDecorator(@NotNull PropertiesSource source) {
        toDecorate = source;
    }

    @Override
    public <T> T get(String name) {
        return toDecorate.get(name);
    }

    @Override
    public void set(String name, Object value) {
        toDecorate.set(name, value);
    }

    @Override
    public void save() {
        toDecorate.save();
    }

    public int getInt(String name) {
        return Integer.parseInt(get(name));
    }
    public boolean getBoolean(String name) {
        return Boolean.parseBoolean(get(name));
    }
    public float getFloat(String name) {
        return Float.parseFloat(get(name));
    }
    public double getDouble(String name) {
        return Double.parseDouble(get(name));
    }
    public String getString(String name) { return get(name).toString(); }

    private PropertiesSource toDecorate;
}
