package com.dreamarts.quick.API.Utils.Validation;

import org.junit.Test;

import static org.junit.Assert.*;

public class ConstrainsTest {

    @Test
    public void NOT_EMPTY_NULL() throws Exception {
        assertNull(Check.one(" ", Constrains.NOT_EMPTY));
        assertNull(Check.one(" ", Constrains.NOT_NULL));

        assertEquals(Constrains.NOT_EMPTY, Check.one("", Constrains.NOT_EMPTY));
        assertEquals(Constrains.NOT_EMPTY, Check.one(null, Constrains.NOT_EMPTY));
        assertEquals(Constrains.NOT_EMPTY, Check.one(null, Constrains.NOT_EMPTY, Constrains.NOT_NULL));
        assertEquals(Constrains.NOT_NULL, Check.one(null, Constrains.NOT_NULL, Constrains.NOT_EMPTY));
    }

    @Test
    public void EMAIL() throws Exception {
        assertNull(Check.one("123@123.com", Constrains.EMAIL));
        assertNull(Check.one("123@123.com.123.dsm.com", Constrains.EMAIL));

        assertEquals(Constrains.EMAIL, Check.one("@123.com", Constrains.EMAIL));
        assertEquals(Constrains.EMAIL, Check.one("12@12", Constrains.EMAIL));
        assertEquals(Constrains.EMAIL, Check.one("123.com", Constrains.EMAIL));
        assertEquals(Constrains.EMAIL, Check.one("", Constrains.EMAIL));
        assertEquals(Constrains.NOT_EMPTY, Check.one(null, Constrains.NOT_EMPTY, Constrains.EMAIL));
    }
    @Test
    public void HAS_NUMBER() throws Exception {
        assertNull(Check.one("abc1abc", Constrains.HAS_NUMBER));
        assertEquals(Constrains.HAS_NUMBER, Check.one("abcabc", Constrains.HAS_NUMBER));
    }
    @Test
    public void WITHOUT_SPACE() throws Exception {
        assertNull(Check.one("abcabc", Constrains.WITHOUT_SPACE));
        assertEquals(Constrains.WITHOUT_SPACE, Check.one(" abcabc", Constrains.WITHOUT_SPACE));
    }
    @Test
    public void HAS_UPPER() throws Exception {
        assertNull(Check.one("aBcabc", Constrains.HAS_UPPER));
        assertEquals(Constrains.HAS_UPPER, Check.one(" abcabc", Constrains.HAS_UPPER));
    }

    @Test
    public void MIN_MAX_LENGTH() throws Exception {
        String string = "123456";
        for (int i = 0; i < 7; i++) {
            assertNull(Check.one(string, Constrains.MIN_LENGTH(i)));
        }
        for (int i = 7; i < 10; i++) {
            assertEquals(Constrains.MIN_LENGTH(i), Check.one(string, Constrains.MIN_LENGTH(i)));
        }

        for (int i = 0; i < 6; i++) {
            assertEquals(Constrains.MAX_LENGTH(i), Check.one(string, Constrains.MAX_LENGTH(i)));
        }
        for (int i = 6; i < 10; i++) {
            assertNull(Check.one(string, Constrains.MAX_LENGTH(i)));
        }

        for (int i = 0; i < 7; i++) {
            for (int j = 6; j < 10; j++) {
                assertNull(Check.one(string, Constrains.MIN_LENGTH(i), Constrains.MAX_LENGTH(j)));
            }
            for (int j = i; j < 6; j++) {
                assertEquals(Constrains.MAX_LENGTH(j), Check.one(string, Constrains.MIN_LENGTH(i), Constrains.MAX_LENGTH(j)));
            }
        }
        for (int i = 7; i < 10; i++) {
            for (int j = 6; j < 10; j++) {
                assertEquals(Constrains.MIN_LENGTH(i), Check.one(string, Constrains.MIN_LENGTH(i), Constrains.MAX_LENGTH(j)));
            }
            for (int j = i; j < 6; j++) {
                assertEquals(Constrains.MIN_LENGTH(i), Check.one(string, Constrains.MIN_LENGTH(i), Constrains.MAX_LENGTH(j)));
                assertEquals(Constrains.MAX_LENGTH(j), Check.one(string, Constrains.MIN_LENGTH(i), Constrains.MAX_LENGTH(j)));
            }
        }
    }

    @Test
    public void MIN_MAX() throws Exception {
        int value = 6;
        for (int i = 0; i < 7; i++) {
            assertNull(Check.one(value, Constrains.MIN(i)));
        }
        for (int i = 7; i < 10; i++) {
            assertEquals(Constrains.MIN(i), Check.one(value, Constrains.MIN(i)));
        }

        for (int i = 0; i < 6; i++) {
            assertEquals(Constrains.MAX(i), Check.one(value, Constrains.MAX(i)));
        }
        for (int i = 6; i < 10; i++) {
            assertNull(Check.one(value, Constrains.MAX(i)));
        }

        for (int i = 0; i < 7; i++) {
            for (int j = 6; j < 10; j++) {
                assertNull(Check.one(value, Constrains.MIN(i), Constrains.MAX(j)));
            }
            for (int j = i; j < 6; j++) {
                assertEquals(Constrains.MAX(j), Check.one(value, Constrains.MIN(i), Constrains.MAX(j)));
            }
        }
        for (int i = 7; i < 10; i++) {
            for (int j = 6; j < 10; j++) {
                assertEquals(Constrains.MIN(i), Check.one(value, Constrains.MIN(i), Constrains.MAX(j)));
            }
            for (int j = i; j < 6; j++) {
                assertEquals(Constrains.MIN(i), Check.one(value, Constrains.MIN(i), Constrains.MAX(j)));
                assertEquals(Constrains.MAX(j), Check.one(value, Constrains.MIN(i), Constrains.MAX(j)));
            }
        }
    }

    @Test
    public void invert() throws Exception {
        assertNull(Check.one(null, Constrains.INVERT(Constrains.NOT_NULL)));
        assertNull(Check.one(null, Constrains.INVERT(Constrains.NOT_EMPTY)));
        assertNull(Check.one("", Constrains.INVERT(Constrains.NOT_EMPTY)));

        assertEquals(
                Constrains.INVERT(Constrains.NOT_NULL),
                Check.one(" ", Constrains.INVERT(Constrains.NOT_NULL)));

        assertEquals(
                Constrains.INVERT(Constrains.MIN(10)),
                Check.one(10, Constrains.INVERT(Constrains.MIN(10))));
        assertEquals(
                Constrains.INVERT(Constrains.MAX(10)),
                Check.one(10, Constrains.INVERT(Constrains.MAX(10))));

        assertNull(Check.one(9, Constrains.INVERT(Constrains.MIN(10))));
        assertNull(Check.one(11, Constrains.INVERT(Constrains.MAX(10))));
    }

}