package com.dreamarts.quick.API.Utils.Email;

public interface HTMLMailSender {
    boolean send(String to, String html);
}
