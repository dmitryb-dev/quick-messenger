package com.dreamarts.quick.API.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.IContext;
import org.thymeleaf.templateresolver.TemplateResolver;

@Component
public class Templates {

    public String parseTemplate(String template, IContext context) {
        return templateEngine.process(template, context);
    }

    @Autowired
    private TemplateEngine templateEngine;
}
