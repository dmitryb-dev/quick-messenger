package com.dreamarts.quick.API.Errors.Core;

import com.dreamarts.quick.API.Utils.JSONStringBuilder;

public class Error {
    private int code;
    private ErrorType type;
    private String guilty, message;

    public Error(int code, String guilty) {
        this(code, ErrorType.Other, guilty, guilty);
    }
    public Error(int code, String guilty, String message) {
        this(code, ErrorType.Other, guilty, message);
    }

    public Error(int code, ErrorType type, String guilty) {
        this(code, type, guilty, guilty);
    }
    public Error(int code, ErrorType type, String guilty, String message) {
        this.code = code;
        this.type = type;
        this.guilty = guilty;
        this.message = message;
    }

    public String toJSON() {
        return new JSONStringBuilder(100)
                .startObject("error")
                .property("code", code)
                .property("type", type.toString())
                .property("guilty", guilty)
                .property("message", message)
                .endObject()
                .toString();
    }

    @Override
    public String toString() {
        return toJSON();
    }

    public int getCode() {
        return code;
    }

    public ErrorType getType() {
        return type;
    }

    public String getGuilty() {
        return guilty;
    }

    public String getMessage() {
        return message;
    }
}