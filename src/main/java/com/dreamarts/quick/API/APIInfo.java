package com.dreamarts.quick.API;

import com.dreamarts.quick.Controllers.Path;

public class APIInfo {
    public static final String PATH = Path.apiPath + Path.apiLevel1;
    public static String path(String controller) {
        return PATH + controller;
    }
}
