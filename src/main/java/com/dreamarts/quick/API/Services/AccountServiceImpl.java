package com.dreamarts.quick.API.Services;

import com.dreamarts.quick.API.DAO.Interfaces.UserDAO;
import com.dreamarts.quick.API.Services.Interfaces.AccountService;
import com.dreamarts.quick.API.Services.Interfaces.NonActivatedService;
import com.dreamarts.quick.Model.NonActivated;
import com.dreamarts.quick.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    @Override
    public User byEmail(String email) {
        return userDAO.byEmail(email);
    }

    @Override
    public User create(String email, String pass) {
        userDAO.create(new User(email, pass));
        return byEmail(email);
    }

    @Override
    public void update(User u) {

    }

    @Override
    public String registerUser(String email, String pass) {
        return nonActivatedService.create(email, pass);
    }

    @Override
    public NonActivated confirm(String email, String code) {
        return nonActivatedService.activate(email, code);
    }

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private NonActivatedService nonActivatedService;
}
