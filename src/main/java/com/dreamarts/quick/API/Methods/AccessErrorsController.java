package com.dreamarts.quick.API.Methods;

import com.dreamarts.quick.API.APIInfo;
import com.dreamarts.quick.API.APIResult;
import com.dreamarts.quick.API.Errors.Presets.AccountErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseStatus(HttpStatus.FORBIDDEN)
@RequestMapping(path = APIInfo.PATH + "/access/errors")
public class AccessErrorsController {

    @RequestMapping("pleasesignin")
    public String notLoggedIn() {
        return APIResult.wrapError(AccountErrors.NOT_LOGGED_IN.toJSON());
    }

    @RequestMapping("accessdenied")
    public String notEnoughAuthorities() {
        return APIResult.wrapError(AccountErrors.ACCESS_DENIED.toJSON());
    }
    @RequestMapping("wrongtoken")
    public String wrongToken() {
        return APIResult.wrapError(AccountErrors.WRONG_TOKEN.toJSON());
    }

}
