package com.dreamarts.quick.API.Utils.Validation;

import java.util.LinkedList;
import java.util.List;

public class Check {


    public static <T, R> R one(T forTest, Constrain<? super T>[] constrains, R[] results) {
        int i = 0;
        for (Constrain constrain: constrains) {
            if (!constrain.test(forTest)) return (R) results[i];
            i++;
        }
        return null;
    }



    public static <T, R> List<R> all(T forTest, Constrain<? super T>[] constrains, R[] results) {
        List<R> infractions = null;
        int i = 0;
        for (Constrain constrain: constrains) {
            if (!constrain.test(forTest)) {
                if (infractions == null) infractions = new LinkedList<>();
                infractions.add((R) results[i]);
            }
            i++;
        }
        return infractions;
    }

    /*
        Definitions for handy calls.
     */
    public static <T> Constrain one(T forTest, Constrain<? super T>... constrains) {
        return (Constrain) one(forTest, constrains, constrains);
    }
    public static <T> List<Constrain> all(T forTest, Constrain<? super T>... constrains) {
        return all(forTest, constrains, constrains);
    }

    public static <T, R> R one(T forTest,
                               Constrain<? super T> constrain1, R ifFail1,
                               Constrain<? super T> constrain2, R ifFail2,
                               Constrain<? super T> constrain3, R ifFail3) {
        return (R) one(forTest,
                new Constrain[] { constrain1, constrain2, constrain3 },
                new Object[] { ifFail1, ifFail2, ifFail3 });
    }
    public static <T, R> R one(T forTest,
                               Constrain<? super T> constrain1, R ifFail1,
                               Constrain<? super T> constrain2, R ifFail2) {
        return (R) one(forTest,
                new Constrain[] { constrain1, constrain2 },
                new Object[] { ifFail1, ifFail2 });
    }

    public static <T, R> List<R> all(T forTest,
                                     Constrain<? super T> constrain1, R ifFail1,
                                     Constrain<? super T> constrain2, R ifFail2,
                                     Constrain<? super T> constrain3, R ifFail3) {
        return (List<R>) all(forTest,
                new Constrain[] { constrain1, constrain2, constrain3 },
                new Object[] { ifFail1, ifFail2, ifFail3 });
    }
    public static <T, R> List<R> all(T forTest,
                               Constrain<? super T> constrain1, R ifFail1,
                               Constrain<? super T> constrain2, R ifFail2) {
        return (List<R>) all(forTest,
                new Constrain[] { constrain1, constrain2 },
                new Object[] { ifFail1, ifFail2 });
    }

}
