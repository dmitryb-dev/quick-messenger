package com.dreamarts.quick.API.Utils.Email;

public interface EmailSender {
    boolean send(String receiversEmail, String text, String subject, boolean isHTML);
}
