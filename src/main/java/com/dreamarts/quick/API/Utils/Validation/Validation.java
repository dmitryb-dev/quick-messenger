package com.dreamarts.quick.API.Utils.Validation;

import java.util.ArrayList;

import java.util.List;

public class Validation {

    public static <T, E> ValidationBuilder<T, E> verify(T toVerify) {
        return new ValidationBuilder<T, E>(toVerify);
    }

    public static class ValidationBuilder<T, E> {
        private List<Constrain> constrains = new ArrayList<>(7);
        private List errors = new ArrayList(7);
        private T toVerify;

        private ValidationBuilder(T toVerify) {
            this.toVerify = toVerify;
        }

        public ValidationBuilder limit(Constrain<? super T> constrain) {
            addErrorForLastConstrain();
            errorObject = constrain;
            constrains.add(constrain);
            return this;
        }
        public ValidationBuilder onError(E error) {
            this.errorObject = error;
            return this;
        }

        public E untilFail() {
            addErrorForLastConstrain();
            return (E) Check.one(toVerify,
                    constrains.toArray(new Constrain[constrains.size()]),
                    errors.toArray());
        }
        public E allErrors() {
            addErrorForLastConstrain();
            return (E) Check.all(toVerify,
                    constrains.toArray(new Constrain[constrains.size()]),
                    errors.toArray());
        }

        private void addErrorForLastConstrain() {
            if (constrains.size() > 0) errors.add(errorObject);
        }
        private Object errorObject;
    }
}
