package com.dreamarts.quick.Model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "nonactivated")
public class NonActivated {
    @Id
    @Column(name = "email")
    private String email;
    @Column(name = "pass")
    private String pass;
    @Column(name = "code")
    private String activationCode;
    @Column(name = "expire")
    private Timestamp expire;

    public NonActivated() {
    }

    public NonActivated(String email, String pass, String activationCode, int lifeTimeSec) {
        this.email = email;
        this.pass = pass;
        this.activationCode = activationCode;
        this.expire = new Timestamp(System.currentTimeMillis() + lifeTimeSec * 1000);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Timestamp getExpire() {
        return expire;
    }

    public void setExpire(Timestamp expire) {
        this.expire = expire;
    }
}
