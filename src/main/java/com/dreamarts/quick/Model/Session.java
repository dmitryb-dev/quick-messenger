package com.dreamarts.quick.Model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "sessions", indexes = @Index(name = "sessionByToken", columnList = "token"))
public class Session {
    public Session(User user, String token, Timestamp expires, Access role) {
        this.user = user;
        this.token = token;
        this.expires = expires;
        this.role = role;
    }

    public Session() {
    }

    @Id
    @Column(name = "id")
    @GeneratedValue()
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @Column(name = "token")
    private String token;

    @Column(name = "expires")
    private Timestamp expires;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Access role;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    public Access getRole() {
        return role;
    }

    public void setRole(Access role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
