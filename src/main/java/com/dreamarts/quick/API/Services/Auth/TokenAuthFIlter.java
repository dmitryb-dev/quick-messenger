package com.dreamarts.quick.API.Services.Auth;

import com.dreamarts.quick.API.Services.Auth.AuthenticationObjects.Token;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthFilter implements Filter{

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filters) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context.getAuthentication() == null || !context.getAuthentication().isAuthenticated()) {
            Authentication parsed = parseAuthentication(request);
            if (parsed != null) SecurityContextHolder.getContext().setAuthentication(parsed);
        }
        filters.doFilter(request, response);
    }

    private Authentication parseAuthentication(ServletRequest request) {
        String tokenInRequest = request.getParameter(TOKEN_PARAM);
        return new Token(tokenInRequest != null? tokenInRequest : parseTokenFromCookies(request));
    }

    private String parseTokenFromCookies(ServletRequest request) {
        if (request instanceof HttpServletRequest) {
            Cookie[] cookies = ((HttpServletRequest) request).getCookies();
            if (cookies == null) return null;
            for (Cookie cookie : cookies)
                if (cookie.getName().equals(TOKEN_PARAM)) return cookie.getValue();
        }
        return null;
    }

    public final static String TOKEN_PARAM = "token";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}
