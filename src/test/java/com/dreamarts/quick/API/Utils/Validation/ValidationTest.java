package com.dreamarts.quick.API.Utils.Validation;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationTest {
    @Test
    public void verify() throws Exception {
        Object result = Validation.verify("abcd")
                .limit(Constrains.MIN_LENGTH(6)).onError(1)
                .untilFail();
        assertEquals(1, result);

        result = Validation.verify("abcd")
                .limit(Constrains.MIN_LENGTH(6))
                .untilFail();
        assertEquals(Constrains.MIN_LENGTH(6), result);

        result = Validation.verify("abcddf")
                .limit(Constrains.MIN_LENGTH(6)).onError(1)
                .limit(Constrains.HAS_NUMBER).onError(2)
                .untilFail();
        assertEquals(2, result);

        result = Validation.verify("abcddf")
                .limit(Constrains.MIN_LENGTH(6)).onError(1)
                .limit(Constrains.HAS_NUMBER).onError(2).onError(3)
                .untilFail();
        assertEquals(3, result);

        result = Validation.verify("abcddf")
                .limit(Constrains.MIN_LENGTH(6)).onError(1)
                .limit(Constrains.HAS_NUMBER)
                .untilFail();
        assertEquals(Constrains.HAS_NUMBER, result);

        result = Validation.verify("abcf")
                .onError(0)
                .limit(Constrains.MIN_LENGTH(6)).onError(1).onError(4)
                .limit(Constrains.HAS_NUMBER).onError(2).onError(3)
                .untilFail();
        assertEquals(4, result);

        result = Validation.verify("abcf")
                .onError(0).onError(2)
                .untilFail();
        assertEquals(null, result);
    }

}