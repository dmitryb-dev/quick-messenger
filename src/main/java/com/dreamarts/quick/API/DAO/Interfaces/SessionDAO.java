package com.dreamarts.quick.API.DAO.Interfaces;

import com.dreamarts.quick.Model.Session;
import com.dreamarts.quick.Model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionDAO {
    Session findByToken(String token);
    void expire(Session s);
    void create(Session s);
}
