package com.dreamarts.quick.API.DAO.Interfaces;

import com.dreamarts.quick.Model.User;
import org.springframework.transaction.annotation.Transactional;

public interface UserDAO {
    User byEmail(String email);
    void create(User user);
    void save(User user);
}
