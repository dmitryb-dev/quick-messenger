package com.dreamarts.quick.API.Utils.Properties;

import com.dreamarts.quick.API.Utils.Properties.Parsers.PropertiesParser;
import com.dreamarts.quick.API.Utils.Properties.Sources.ResourceSource;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Properties;

public class FromTest {
    @Test
    public void propertiesFileTest() {
        Properties p = new Properties();
        try {
            p.load(getClass().getClassLoader().getResourceAsStream("test.prop"));
            Assert.assertEquals("old", p.getProperty("test.prop"));
        } catch (IOException e) {
            throw new AssertionError();
        }

        From.append("test", new ResourceSource("test.prop", new PropertiesParser()));
        From.setDefault("test");

        Assert.assertEquals("old", From.source().get("test.prop"));

        From.source().set("test.prop", "test value");
        From.saveAll();
        From.append("opentest", new ResourceSource("test.prop", new PropertiesParser()));

        Assert.assertEquals("test value", From.source("opentest").get("test.prop"));
        Assert.assertEquals("test value", From.source().get("test.prop"));

        From.source("opentest").set("test.prop", "old");
        From.saveAll();
    }
}