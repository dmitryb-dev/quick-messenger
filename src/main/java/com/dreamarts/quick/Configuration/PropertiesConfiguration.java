package com.dreamarts.quick.Configuration;

import com.dreamarts.quick.API.Utils.Properties.From;
import com.dreamarts.quick.API.Utils.Properties.Parsers.PropertiesParser;
import com.dreamarts.quick.API.Utils.Properties.Sources.ResourceSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfiguration {
    public static void init() {
        From.append("mail", new ResourceSource("Email.properties", new PropertiesParser()));
    }
    @Bean
    From propertiesSource() {
        init();
        return new From();
    }
}
