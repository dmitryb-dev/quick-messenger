package com.dreamarts.quick.Model;

public enum Access {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_ANONYMOUS;

    public static Access fromString(String value) {
        for (Access a : Access.values())
            if (a.name().equalsIgnoreCase(value))
                return a;
        return null;
    }

    @Override
    public String toString() {
        return super.name();
    }
}
