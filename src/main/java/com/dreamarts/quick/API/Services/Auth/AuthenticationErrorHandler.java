package com.dreamarts.quick.API.Services.Auth;

import org.springframework.security.core.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthenticationErrorHandler {

    public AuthenticationErrorHandler(String wrongTokenURL, String noTokenURL) {
        this.wrongToken = wrongTokenURL;
        this.noToken = noTokenURL;
    }

    public void handle(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) {
        String url = ex.getClass().equals(WrongSessionException.class)? wrongToken : noToken;
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String wrongToken, noToken;
}
