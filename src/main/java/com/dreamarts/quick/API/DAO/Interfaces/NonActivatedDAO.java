package com.dreamarts.quick.API.DAO.Interfaces;


import com.dreamarts.quick.Model.NonActivated;
import org.springframework.data.repository.CrudRepository;

public interface NonActivatedDAO extends CrudRepository<NonActivated, String> {
}
