package com.dreamarts.quick.API;

import com.dreamarts.quick.API.Utils.JSONStringBuilder;

public class APIResult {

    public static String wrap(String json) {
        return new JSONStringBuilder(1000)
                .startObject("response")
                .rawText(json)
                .endObject()
                .toString();
    }

    public static String wrap(String result, String json) {
        return wrap(
                new JSONStringBuilder(1000)
                        .property("result", result)
                        .rawText(json)
                        .toString());
    }

    public static String wrapOK(String json) {
        return wrap("OK", json);
    }
    public static String wrapError(String json) {
        return wrap("ERROR", json);
    }
    public static String RESULT_OK = wrap("\"result\": \"OK\"");

    public enum Result {
        OK, ERROR;

        @Override
        public String toString() {
            return this.name();
        }
    }

    public static APIResult wrapOK(Object payload) {
        return new APIResult(Result.OK, payload);
    }
    public static APIResult wrapError(Object payload) {
        return new APIResult(Result.ERROR, payload);
    }

    public APIResult(Result result, Object payload) {
        this.result = result;
        this.payload = payload;
    }

    private Result result;
    private Object payload;

    public Result getResult() {
        return result;
    }
    public Object getPayload() {
        return payload;
    }
}
