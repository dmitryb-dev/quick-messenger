package com.dreamarts.quick.API;

import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import static com.dreamarts.quick.Controllers.Path.*;

@Controller
public class APIController {
    @RequestMapping(path = finalVersionApi + "/**")
    public String lastAPIVersion(HttpServletRequest request) {
        return redirectToApiVersion(apiLevel1, request);
    }

    @RequestMapping(path = defaultVersionApi + "/**")
    public String defaultAPIVersion(HttpServletRequest request) {
        return lastMajorAPIVersion(1, request);
    }

    @RequestMapping(path = lastMajorApi + "{version}/**")
    public String lastMajorAPIVersion(@PathVariable int version, HttpServletRequest request) {
        switch (version) {
            case 1: return redirectToApiVersion(apiLevel1, request);
        }
        return defaultAPIVersion(request);
    }

    private String redirectToApiVersion(String apiPath, HttpServletRequest request) {
        final String url = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
    //    final String params = request.getQueryString() != null? "?" + request.getQueryString() : "";
        final String bestMatch = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        final String method = pathMatcher.extractPathWithinPattern(bestMatch, url);
        return "forward:/" + apiPath + method;
    }

    private static final AntPathMatcher pathMatcher = new AntPathMatcher();
}
