package com.dreamarts.quick.API.Errors.Core;

public enum ErrorType {
    Account("Account error"),
    Server("Internal server error"),
    Other("Untyped");

    private String type;
    ErrorType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
