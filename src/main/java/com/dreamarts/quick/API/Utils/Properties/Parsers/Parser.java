package com.dreamarts.quick.API.Utils.Properties.Parsers;

import java.util.Map;

public interface Parser {
    Map<String, Object> parse(String source) throws InvalidFormatException;
    String format(Map<String, Object> values);
}
