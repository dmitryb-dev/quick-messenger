package com.dreamarts.quick.API.Utils;

import org.hibernate.Query;

import java.util.List;

public class Queries {
    public static <T> T firstOrNull(Query query) {
        List<T> result = query.list();
        return result.size() > 0? result.get(0) : null;
    }
}
