package com.dreamarts.quick.Controllers;

public class Path {
    public static final String
        lastMajorApi = "apimajor/";

    public static final String
        finalVersionApi = "apilast/";

    public static final String
        defaultVersionApi = "api/";

    public static final String apiPath = "";

    public static final String
            apiLevel1 = "apiversion/1.0/";

    public static final String[] apiLevels = {
            apiLevel1
    };
}
