package com.dreamarts.quick.API.DAO;

import com.dreamarts.quick.API.DAO.Interfaces.UserDAO;
import com.dreamarts.quick.API.Utils.Queries;
import com.dreamarts.quick.Model.User;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
public class UserDAOImpl implements UserDAO {
    @Override
    @Transactional(readOnly = true)
    public User byEmail(String email) {
        Session session = (Session) entityManager.getDelegate();
        return Queries.firstOrNull(
                session.createQuery("from User u where u.email = '" + email + "'")
        );
    }

    @Override
    @Transactional
    public void create(User user) {
        Session session = (Session) entityManager.getDelegate();
        session.save(user);
    }

    @Override
    public void save(User user) {
    }

    @Autowired
    private EntityManager entityManager;
}
