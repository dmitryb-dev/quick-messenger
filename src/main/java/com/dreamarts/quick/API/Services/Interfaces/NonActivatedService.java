package com.dreamarts.quick.API.Services.Interfaces;

import com.dreamarts.quick.Model.NonActivated;

public interface NonActivatedService {
    String create(String email, String pass);
    NonActivated activate(String email, String code);
}
