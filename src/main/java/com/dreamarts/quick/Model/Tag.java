package com.dreamarts.quick.Model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tags")
public class Tag {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;

    @Column(name = "tag")
    private String tag;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "userTags", joinColumns = @JoinColumn(name = "tagId"), inverseJoinColumns = @JoinColumn(name = "userId"))
    private List<User> users;
}
