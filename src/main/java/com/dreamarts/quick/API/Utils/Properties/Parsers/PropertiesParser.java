package com.dreamarts.quick.API.Utils.Properties.Parsers;

import java.io.*;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesParser implements Parser {
    @Override
    public Map<String, Object> parse(String source) throws InvalidFormatException {
        Properties properties = new Properties();
        try {
            properties.load(new ByteArrayInputStream(source.getBytes()));
        } catch (IOException e) {
            throw new InvalidFormatException("Can't read source");
        }

        Map<String, Object> map = new HashMap<>();
        for (Object key : properties.keySet())
            map.put(key.toString(), properties.get(key));
        return map;
    }

    @Override
    public String format(Map<String, Object> values) {
        Properties properties = new Properties();
        for (Map.Entry pair : values.entrySet())
            properties.put(pair.getKey(), pair.getValue());

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            properties.store(stream, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String formatted = null;
        try {
            formatted = stream.toString(Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return formatted;
    }
}
