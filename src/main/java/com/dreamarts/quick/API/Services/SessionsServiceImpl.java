package com.dreamarts.quick.API.Services;

import com.dreamarts.quick.API.DAO.Interfaces.SessionDAO;
import com.dreamarts.quick.API.Services.Interfaces.SessionsService;
import com.dreamarts.quick.API.Utils.Utils;
import com.dreamarts.quick.Model.Session;
import com.dreamarts.quick.Model.User;
import com.dreamarts.quick.Model.Access;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class SessionsServiceImpl implements SessionsService {
    @Override
    public Session startNew(User u, Access access, int validityDays) {
        long expirationTime = System.currentTimeMillis() + validityDays * Utils.MILLIS_IN_DAY;
        Session session = new Session(u, generateToken(u), new Timestamp(expirationTime), access);
        sessionDAO.create(session);
        return session;
    }

    @Override
    public void expire(Session s) {
        sessionDAO.expire(s);
    }

    public String generateToken(User u) {
        return Utils.randomString(32)
                + u.getId()
                + new BCryptPasswordEncoder().encode(u.getEmail() + u.getPass());
    }

    @Autowired
    private SessionDAO sessionDAO;
}
