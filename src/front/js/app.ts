import Pages from "../node_modules/pagesfw/js/Pages";
import PagesFactory from "../node_modules/pagesfw/js/PagesFactory";
import {Animation} from "../node_modules/pagesfw/js/core/Pages/Animation";

const pages: Pages = PagesFactory.fromDocument(PagesFactory.slashMode("app"));

pages.container('account').page('Sign Up').to('Sign In').animation =
    new Animation('fadeInLeft animated', 'fadeOutRight animated');

pages.container('main').page('Account Info').to('Authorization').animation =
    new Animation('rotateIn animated', 'fadeOutDown animated');


/*
 const exploders = new ExplodersParser();
exploders.from(document);
const exploder = exploders.get("info");
exploder.onLoad = () => pages.parse(exploder.container);
pages.container('main').page('Account Info').onSelect = () => exploder.explode();
*/



pages.setIndex('Authorization/Sign In');
pages.start();