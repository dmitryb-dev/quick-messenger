package com.dreamarts.quick.API.Errors.Core;

import java.lang.*;

public abstract class AbstractErrors {
    protected static ErrorType type;
    protected static Error error(int code, String guilty, String message) {
        return new Error(code, type, guilty, message);
    }
}
