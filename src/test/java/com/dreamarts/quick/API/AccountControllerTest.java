package com.dreamarts.quick.API;

import com.dreamarts.quick.API.Errors.Presets.AccountErrors;
import com.dreamarts.quick.API.Methods.AccountController;
import com.dreamarts.quick.Configuration.PropertiesConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountControllerTest {
    @Autowired
    private AccountController controller;

    static {
        PropertiesConfiguration.init();
    }

    private int getErrorCode(String errorJSON) {
        Matcher matcher = Pattern.compile("(?<=\"code\":\\s?)\\d+").matcher(errorJSON);
        if (matcher.find())
            return Integer.parseInt(matcher.group());
        return -1;
    }

    @Test
    public void signIn() throws Exception {

    }

    @Test
    public void signOut() throws Exception {

    }

    @Test
    public void signUp() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        assertEquals(
                -1,
                getErrorCode(controller.signUp("email@email.com", "abcdef123", request))
        );
        assertEquals(
                AccountErrors.WRONG_EMAIL_FORMAT.getCode(),
                getErrorCode(controller.signUp("emailemai", "abcdef123", request))
        );
        assertEquals(
                AccountErrors.WRONG_EMAIL_FORMAT.getCode(),
                getErrorCode(controller.signUp("emai@lemai", "abcdef123", request))
        );
        assertEquals(
                AccountErrors.SHORT_PASSWORD.getCode(),
                getErrorCode(controller.signUp("email@email.com", "egd12", request))
        );
        assertEquals(
                AccountErrors.MUST_HAVE_NUMBER.getCode(),
                getErrorCode(controller.signUp("email@email.com", "abcdefdf", request))
        );
    }

}