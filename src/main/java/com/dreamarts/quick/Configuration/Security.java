package com.dreamarts.quick.Configuration;

import com.dreamarts.quick.API.Services.Auth.AuthenticationErrorHandler;
import com.dreamarts.quick.API.Services.Auth.TokenAuthFilter;
import com.dreamarts.quick.API.Services.Auth.TokenAuthProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/apiversion/*/account/signup",
                "/apiversion/*/account/signin",
                "/apiversion/*/account/activate",
                "/apiversion/*/account/errors/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final AuthenticationErrorHandler errorHandler = new AuthenticationErrorHandler(
                "/apiversion/1.0/access/errors/wrongtoken", "/apiversion/1.0/access/errors/pleasesignin");

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.antMatcher("/apiversion/**")
                .addFilterBefore(new TokenAuthFilter(), LogoutFilter.class)
                .exceptionHandling()
                    .accessDeniedPage("/apiversion/1.0/access/errors/accessdenied")
                    .authenticationEntryPoint(errorHandler::handle).and()
                .authorizeRequests()
                    .antMatchers("/apiversion/**").authenticated();
    }

    @Autowired
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(provider);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private TokenAuthProvider provider;
}
