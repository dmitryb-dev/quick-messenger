package com.dreamarts.quick.API.Utils.Email;

import com.dreamarts.quick.API.Utils.Properties.From;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class EmailSenderImpl implements EmailSender {
    @Override
    public boolean send(String receiversEmail, String text, String subject, boolean isHTML) {
        MimeMessageHelper message = new MimeMessageHelper(mailSender.createMimeMessage());
        try {
            message.setFrom(From.source("mail").getString("from"));
            message.setTo(receiversEmail);
            message.setText(text, isHTML);
            message.setSubject(subject);
            mailSender.send(message.getMimeMessage());
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Autowired
    @Qualifier("localMailSender")
    private JavaMailSender mailSender;
}
