package com.dreamarts.quick.API.Errors.Presets;

import com.dreamarts.quick.API.Errors.Core.AbstractErrors;
import com.dreamarts.quick.API.Errors.Core.Error;
import com.dreamarts.quick.API.Errors.Core.ErrorType;

public class AccountErrors extends AbstractErrors {
    static {
        // Code 1**
        type = ErrorType.Account;
    }

    // Sign up errors, guilty - email, code 11*
    public static final Error
            WRONG_EMAIL_FORMAT = error(111, "Email", "Email doesn't match pattern *@*.*"),
            EMAIL_EXISTS = error(112, "Email", "User with this email already exists"),

    // Sign up errors, guilty - password, code 12*
            SHORT_PASSWORD = error(121, "Password", "Password must be longer than 6 symbols"),
            MUST_HAVE_NUMBER = error(122, "Password", "Password must have at least one digit");

    // Sign in errors, code 13*
    public static final Error
            WRONG_EMAIL = error(131, "Email", "No user with very email"),
            WRONG_PASS = error(132, "Password", "Wrong password");

    // Access errors, code 14*
    public static final Error
            NOT_LOGGED_IN = error(141, "User", "You are not logged in"),
            WRONG_TOKEN = error(142, "User", "Token is wrong or session expired"),
            ACCESS_DENIED = error(143, "User", "You don't have authorities for this action");

    public static final Error
            WRONG_CONFIRMATION_CODE = error(151, "User", "Wrong confirmation code");

}
