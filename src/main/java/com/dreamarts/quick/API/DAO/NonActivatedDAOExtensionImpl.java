package com.dreamarts.quick.API.DAO;

import com.dreamarts.quick.API.DAO.Interfaces.NonActivatedDAOExtension;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import java.sql.Timestamp;

@Repository
public class NonActivatedDAOExtensionImpl implements NonActivatedDAOExtension {
    @Override
    @Transactional
    public void expire() {
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Timestamp now = new Timestamp(System.currentTimeMillis());
        sessionFactory.getCurrentSession().createQuery("delete NonActivated a where a.expire < " + now);
    }

    @Autowired
    private EntityManagerFactory entityManagerFactory;
}
