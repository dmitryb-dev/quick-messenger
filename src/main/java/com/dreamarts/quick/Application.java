package com.dreamarts.quick;

import com.dreamarts.quick.Configuration.PropertiesConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude=HibernateJpaAutoConfiguration.class)
public class Application {

	public static void main(String[] args) {
		PropertiesConfiguration.init();
		SpringApplication.run(Application.class, args);
	}
}
