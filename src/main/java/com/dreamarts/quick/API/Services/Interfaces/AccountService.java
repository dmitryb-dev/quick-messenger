package com.dreamarts.quick.API.Services.Interfaces;

import com.dreamarts.quick.Model.NonActivated;
import com.dreamarts.quick.Model.User;

public interface AccountService {
    User byEmail(String email);
    User create(String email, String pass);
    void update(User u);

    String registerUser(String email, String pass);

    NonActivated confirm(String email, String code);
}
